from django.apps import AppConfig


class BookdozesConfig(AppConfig):
    name = 'bookdozes'
