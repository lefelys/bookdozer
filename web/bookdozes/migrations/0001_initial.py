# Generated by Django 2.0 on 2018-01-23 10:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('book_name', models.CharField(max_length=500, verbose_name='Book name')),
                ('book_data', models.TextField(verbose_name='Full book text')),
                ('date', models.DateField(verbose_name='Publication date')),
            ],
        ),
        migrations.CreateModel(
            name='Bookpart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('part_number', models.BigIntegerField(verbose_name='Part number')),
                ('part_text', models.TextField(verbose_name='Book doze text')),
                ('date', models.DateField(verbose_name='Publication date')),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bookdozes.Book')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('first_name', models.CharField(max_length=255, verbose_name='First name')),
                ('last_name', models.CharField(blank=True, max_length=255, null=True, verbose_name='Last name')),
                ('username', models.CharField(blank=True, max_length=255, null=True, verbose_name='User name')),
            ],
        ),
        migrations.AddField(
            model_name='book',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bookdozes.User'),
        ),
    ]
