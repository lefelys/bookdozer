# Generated by Django 2.0 on 2018-02-16 09:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookdozes', '0002_auto_20180129_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='chat_id',
            field=models.BigIntegerField(default=1),
            preserve_default=False,
        ),
    ]
