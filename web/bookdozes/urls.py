from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:bookpart_book_id>/<int:bookpart_part_number>/', views.doze, name='doze'),
]
