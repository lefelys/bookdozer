from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Bookpart
from django.template import loader, engines
from .models import Book

def index(request):

    return HttpResponse('Bookdozes index')


def doze(request, bookpart_book_id, bookpart_part_number):
    query = get_object_or_404(Bookpart, book = bookpart_book_id, part_number = bookpart_part_number)
    part = query.part_text
    django_engine = engines['django']
    subTemplateString = '{% load static %}\n' + part
    subtemplate = django_engine.from_string(subTemplateString)
    template = loader.get_template('bookdozes/bookdozes.html')

    context = {
        'book_part_data': query,
        'partRendered': subtemplate.render()
    }

    return HttpResponse(template.render(context, request))
