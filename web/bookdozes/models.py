from django.db import models

# Create your models here.


class User(models.Model):
    id = models.BigIntegerField(primary_key=True)
    first_name = models.CharField('First name', max_length=255)
    last_name = models.CharField('Last name', max_length=255, blank=True, null=True)
    username = models.CharField('User name', max_length=255, blank=True, null=True)
    def __str__(self):
        return self.first_name

class Book(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chat_id = models.BigIntegerField()
    book_name = models.CharField('Book name', max_length=500)
    book_data = models.TextField('Full book text')
    date = models.DateField('Publication date')
    status = models.BooleanField('Book status', default = False)
    readtime = models.IntegerField('Time user wants to read', null = True)
    def __str__(self):
        return self.book_name

class Bookpart(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    part_number = models.BigIntegerField('Part number')
    part_text = models.TextField('Book doze text')
    date = models.DateField('Publication date')
    def __str__(self):
        return self.part_text
