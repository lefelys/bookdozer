
# Bookdozer
### Telegram-bot for daily book-reading

[http://bookdozer.lefelys.com](http://bookdozer.lefelys.com)

Create telegram-subscription from epub-book and read it in convenient instant-view format: [@BookdozerBot](https://telegram.me/BookdozerBot)

Modules (Docker-containers):

* **/bot_celery:** bot back-end, book processing
* **/web:** django back-end (instant-view pages, landing-page)
* **/nginx:** web-server

Images:

* **RabbitMQ:** message broker (bot <> celery)
* **PostgreSQL:** database


