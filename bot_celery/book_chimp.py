import psycopg2
import os
from datetime import datetime

'''
1 minute: ~1800 chars
'''

database = os.environ['DB_NAME']
user = os.environ['DB_USER']
password = os.environ['DB_PASS']
host = os.environ['DB_SERVICE']
port = os.environ['DB_PORT']


def get_books_on_send(connect):
    with connect.cursor() as c:

        c.execute('''
        SELECT id
        FROM bookdozes_book
        WHERE Status = %s;
        ''', (True,))

        data = c.fetchall()
    if data:
        return data
    else:
        return False


def get_book_data__readtime__chat_id(connect, bookID):
    with connect.cursor() as c:

        c.execute('''
        SELECT book_data, readtime, chat_id
        FROM bookdozes_book
        WHERE id = %s;
        ''', (bookID,))
        data = c.fetchone()

    if data:
        return data[0], data[1], data[2]
    else:
        return False, False, False


def update_container__add_part(connect, bookID, new_book_data, part):
    with connect.cursor() as c:

        c.execute('''
        UPDATE bookdozes_book
        SET book_data = %s
        WHERE id = %s;
        ''', (new_book_data, bookID))

        c.execute('''
        SELECT MAX(part_number)
        FROM bookdozes_bookpart
        WHERE book_id = %s;
        ''', (bookID,))

        h = c.fetchone()
        if h[0]:
            part_number = h[0] + 1
        else:
            part_number = 1
        date = datetime.utcnow().strftime("%Y-%m-%d")

        c.execute('''
        INSERT INTO bookdozes_bookpart (book_id, part_number, part_text, date)
        VALUES (%s, %s, %s, %s);
        ''', (bookID, part_number, part, date))
        # connect.commit(): Not commited for rolling back after bad push

    return part_number

def recreate_book(connect, bookID):
    with connect.cursor() as c:

        c.execute('''
        SELECT book_data
        FROM bookdozes_book
        WHERE id = %s;
        ''', (bookID,))

        remain_book = c.fetchall()

        c.execute('''
        SELECT part_text
        FROM bookdozes_bookpart
        WHERE book_id = %s
        ORDER BY part_number;
        ''', (bookID,))

        parts = c.fetchall()
        full_book = remain_book[0][0] + ''.join([i[0] for i in parts])

        c.execute('''
        UPDATE bookdozes_book
        SET (book_data, status) = (%s, %s)
        WHERE id = %s;
        ''', (full_book, False, bookID))

        c.execute('''
        DELETE FROM bookdozes_bookpart
        WHERE book_id = %s;
        ''', (bookID, ))
        connect.commit()


def end_book(connect, bookID):
    with connect.cursor() as c:
        c.execute('''
        UPDATE bookdozes_book
        SET status = %s
        WHERE id = %s;
        ''', (False, bookID))
        connect.commit()


def splitter(book_data, readtime):
    chars = 1800 * readtime
    if not book_data:
        return False, False
    part, newPart = book_data[:chars], book_data[chars:]
    counter = 0
    lenn = len(newPart)
    while True:
        if counter + 4 >= lenn:
            part += newPart
            newPart = ''
            return newPart, part
        charA, charB, charC = newPart[counter], newPart[counter +
                                                        1], newPart[counter + 2]
        if charA == '/' and charB == 'p' and charC == '>':
            part += newPart[:counter+3]
            newPart = newPart[counter+3:]
            return newPart, part
        else:
            counter += 1
            continue
