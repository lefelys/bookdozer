import sys
import asyncio
import json
import time
import os
import psycopg2
from celery import Celery
import celeryconfig
import logging
from telepot import glance
import telepot.aio
import telepot.aio.helper
from telepot.aio.loop import MessageLoop
from telepot.namedtuple import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telepot.aio.delegate import (per_chat_id, create_open,
                                  pave_event_space, include_callback_query_chat_id)
from book_chimp import (get_books_on_send, update_container__add_part,
                        get_book_data__readtime__chat_id, splitter, end_book, recreate_book)

db = os.environ['DB_NAME']
user = os.environ['DB_USER']
pswrd = os.environ['DB_PASS']
host = os.environ['DB_SERVICE']
port = os.environ['DB_PORT']
bot_token = os.environ['BOT_TOKEN']
rabbit_host = os.environ['RABBIT_HOST']


logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stdout,
    format='%(asctime)s;%(levelname)s;%(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
    )


celery_app = Celery('bookdozer')
celery_app.config_from_object(celeryconfig)

class Reader(telepot.aio.helper.ChatHandler):
    def __init__(self, *args, **kwargs):
        super(Reader, self).__init__(*args, **kwargs)
        self.status = 0
        self.newbooktime = ''
        self.timeDict = {
            '5 min': 5,
            '10 min': 10,
            '20 min': 20,
            '30 min': 30
        }
        self.cancel_keyboard = ReplyKeyboardMarkup(keyboard=[
            ['/cancel'],
        ], resize_keyboard=True)

        self.time_keyboard = ReplyKeyboardMarkup(keyboard=[
            ['5 min', '10 min'],
            ['20 min', '30 min'],
            ['/cancel']
        ], resize_keyboard=True)

        self.tips = (
            '💡 Tip:\nCreate a chat with this bot for every book you want to subscribe to, '
            'name it after a book (add a book cover, maybe) \n💡 Tip 2: \n You can also '
            'add your friends to the chat, and read any book together, like '
            'watching a TV-series!'
        )

    def info_loader(self, msg):
        self.user_id_inst = msg['from'].get('id')
        self.first_name_inst = msg['from'].get('first_name', None)
        self.last_name_inst = msg['from'].get('last_name', None)
        self.username_inst = msg['from'].get('username', None)
        self.chat_id_inst = msg['chat'].get('id')

    async def first_message(self, msg):  # status 0
        msg1 = (
            'Greetings!\nThis smart-ass bot can create subscriptions out of EPUB-books.'
            'You just send him a book, choose how long you want to read it perday, and '
            'he will subscribe you to it!\n/tips for convenient using\n[ / ] for all commands'
        )

        await self.sender.sendMessage(msg1, reply_markup=ReplyKeyboardRemove())

    async def new_book_set_time(self):
        await self.sender.sendMessage('How long do you want to read?', reply_markup=self.time_keyboard)

    async def celery_sender(self, body):
        celery_app.send_task('tasks.book_processor', args=body, kwargs={}, queue='book_processor_queue')

    async def send_books_list(self, task, allOrSubscribed='all'):
        connect = psycopg2.connect(
            database=db, user=user, host=host, password=pswrd, port=port)

        with connect:
            with connect.cursor() as cursor:
                if allOrSubscribed == 'all':
                    cursor.execute('''
                    SELECT id, book_name, readtime, status
                    FROM bookdozes_book
                    WHERE user_id = %s
                    ORDER BY id;
                    ''', (self.user_id_inst, ))
                    data = cursor.fetchall()
                else:
                    cursor.execute('''
                    SELECT id, book_name, readtime, status
                    FROM bookdozes_book
                    WHERE user_id = %s AND status = %s
                    ORDER BY id;
                    ''', (self.user_id_inst, True))
                    data = cursor.fetchall()
        self.idmapping = {x + 1: data[x] for x in range(len(data))}
        form = '/{}. {}\nRead time: {}\nOn sending: {}\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n'
        template = 'Choose a book to ' + task + ':\n\n⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n' + \
            ''.join([form.format(i + 1, data[i][1], data[i][2], data[i][3])
                     for i in range(len(data))])
        connect.close()
        await self.sender.sendMessage(template, reply_markup=self.cancel_keyboard)

    async def change_time(self, oldbookID, oldBookNewTime):
        connect = psycopg2.connect(
            database=db, user=user, host=host, password=pswrd, port=port)
        with connect:
            with connect.cursor() as cursor:
                cursor.execute('''
                UPDATE bookdozes_book
                SET readtime = %s
                WHERE id = %s;
                ''', (oldBookNewTime, oldbookID))
                connect.commit()
        connect.close()

    async def play_pause_book(self, oldBookID, oldBookNewStatus):
        connect = psycopg2.connect(
            database=db, user=user, host=host, password=pswrd, port=port)
        with connect:
            with connect.cursor() as cursor:
                cursor.execute('''
                UPDATE bookdozes_book
                SET status = %s
                WHERE id = %s;
                ''', (oldBookNewStatus, oldBookID))
                connect.commit()
        connect.close()

    async def delete_book(self, oldBookID):
        connect = psycopg2.connect(
            database=db, user=user, host=host, password=pswrd, port=port)
        with connect:
            with connect.cursor() as cursor:

                cursor.execute('''
                DELETE FROM bookdozes_bookpart
                WHERE book_id = %s;
                ''', (oldBookID, ))

                cursor.execute('''
                DELETE FROM bookdozes_book
                WHERE id = %s;
                ''', (oldBookID, ))
                connect.commit()
        connect.close()

    async def send_next_part(self, bookID):
        connect = psycopg2.connect(
            database=db, user=user, host=host, password=pswrd, port=port)
        book_data, readtime, chat_id = get_book_data__readtime__chat_id(
            connect, bookID)
        newContainer, part = splitter(book_data, readtime)
        if not newContainer:
            end_book(connect, bookID)
            recreate_book(connect, bookID)
            return
        part_number = update_container__add_part(
            connect, bookID, newContainer, part)  # not commited
        try:
            await self.sender.sendMessage(
                f'https://t.me/iv?url=http%3A%2F%2Fbookdozer.lefelys.com%2Fbookdozes%2F{bookID}%2F{part_number}%2F&rhash=ac8f7c8eddfba0',
                reply_markup=ReplyKeyboardRemove()
                )

        except Exception as e:
            logging.error(e)
            connect.rollback()  # rolled back update_container__add_part()
        else:
            connect.commit()  # commited update_container__add_part()
        connect.close()


    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        if content_type == 'text':
            if msg['text'] == '/cancel':
                self.close()
            elif msg['text'] == '/tips':
                await self.sender.sendMessage(self.tips)
                self.close()
        if self.status == 0:
            if content_type == 'text':
                self.info_loader(msg)
                if msg['text'] in ['/new', '/new@BookdozerBot']:
                    await self.new_book_set_time()
                    self.status = 11
                elif msg['text'] in ['/update', '/update@BookdozerBot']:
                    await self.send_books_list('update')
                    self.status = 21
                elif msg['text'] in ['/pause', '/pause@BookdozerBot']:
                    await self.send_books_list('play/pause')
                    self.status = 31
                elif msg['text'] in ['/delete', '/delete@BookdozerBot']:
                    await self.send_books_list('delete')
                    self.status = 41
                elif msg['text'] in ['/next', '/next@BookdozerBot']:
                    await self.send_books_list('get next part', 'subscribed')
                    self.status = 51
                    pass
                elif msg['text'] in ['/tips', '/tips@BookdozerBot']:
                    await self.send_books_list(self.tips)
                else:
                    await self.first_message(msg)

        elif self.status == 11:
            if content_type == 'text':
                text = msg['text']
                if text in ['5 min', '10 min', '20 min', '30 min']:
                    self.newbooktime = text
                    await self.sender.sendMessage('Send me a EPUB book', reply_markup=self.cancel_keyboard)
                    self.status = 12
                else:
                    await self.sender.sendMessage('Proper time pls', reply_markup=self.time_keyboard)
            else:
                await self.sender.sendMessage('Proper time pls', reply_markup=self.time_keyboard)

        elif self.status == 12:
            if content_type == 'document':
                if msg['document']['mime_type'] == 'application/epub+zip' and msg['document']['file_size'] <= 20971519:
                    body = [
                        self.user_id_inst,
                        self.first_name_inst,
                        self.last_name_inst,
                        self.username_inst,
                        msg['document']['file_id'],
                        msg['document']['file_name'],
                        self.timeDict[self.newbooktime],
                        self.chat_id_inst,
                        chat_type
                    ]
                    await self.celery_sender(body)
                    await self.sender.sendMessage(
                        "Done! If some problems will occure: we'll notice you",
                        reply_markup=ReplyKeyboardRemove()
                    )
                    self.close()
                else:
                    await self.sender.sendMessage(
                        "It must be EPUB-file with size < 20 mb",
                        reply_markup=self.cancel_keyboard
                    )
            else:
                await self.sender.sendMessage(
                    "It must be EPUB-file with size < 20 mb",
                    reply_markup=self.cancel_keyboard
                )

        elif self.status == 21:
            if content_type == 'text':
                text = msg['text']
                if text in ['/' + str(i) for i in list(self.idmapping)] + \
                        ['/' + str(i) + '@BookdozerBot' for i in list(self.idmapping)]:
                    self.oldbookID = self.idmapping[int(
                        text[1:].replace('@BookdozerBot', ''))][0]
                    await self.sender.sendMessage('Choose new time', reply_markup=self.time_keyboard)
                    self.status = 22
                else:
                    await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)
            else:
                await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)

        elif self.status == 22:
            if content_type == 'text':
                text = msg['text']
                if text in ['5 min', '10 min', '20 min', '30 min']:
                    oldBookNewTime = self.timeDict[msg['text']]
                    await self.change_time(self.oldbookID, oldBookNewTime)
                    await self.sender.sendMessage("Done!", reply_markup=self.cancel_keyboard)
                    self.close()
                else:
                    await self.sender.sendMessage("Proper time pls", reply_markup=self.time_keyboard)
            else:
                await self.sender.sendMessage("Proper time pls", reply_markup=self.time_keyboard)

        elif self.status == 31:
            if content_type == 'text':
                text = msg['text']
                if text in ['/' + str(i) for i in list(self.idmapping)] + \
                        ['/' + str(i) + '@BookdozerBot' for i in list(self.idmapping)]:
                    oldBookID = self.idmapping[int(
                        text[1:].replace('@BookdozerBot', ''))][0]
                    oldBookNewStatus = not self.idmapping[int(
                        text[1:].replace('@BookdozerBot', ''))][3]
                    await self.play_pause_book(oldBookID, oldBookNewStatus)
                    await self.sender.sendMessage("Done!", reply_markup=self.cancel_keyboard)
                    self.close()
                else:
                    await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)
            else:
                await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)

        elif self.status == 41:
            if content_type == 'text':
                text = msg['text']
                if text in ['/' + str(i) for i in list(self.idmapping)] + \
                        ['/' + str(i) + '@BookdozerBot' for i in list(self.idmapping)]:
                    oldBookID = self.idmapping[int(
                        text[1:].replace('@BookdozerBot', ''))][0]
                    await self.delete_book(oldBookID)
                    await self.sender.sendMessage("Done!", reply_markup=self.cancel_keyboard)
                    self.close()
                else:
                    await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)
            else:
                await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)

        elif self.status == 51:
            if content_type == 'text':
                text = msg['text']
                if text in ['/' + str(i) for i in list(self.idmapping)] + \
                        ['/' + str(i) + '@BookdozerBot' for i in list(self.idmapping)]:
                    oldBookID = self.idmapping[int(
                        text[1:].replace('@BookdozerBot', ''))][0]
                    await self.send_next_part(oldBookID)
                    self.close()
                else:
                    await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)
            else:
                await self.sender.sendMessage("Proper book pls", reply_markup=self.cancel_keyboard)


    async def on__idle(self, event):
        self.close()

    async def on_close(self, ex):
        await self.sender.sendMessage("see ya!", reply_markup=ReplyKeyboardRemove())


if __name__ == '__main__':

    bot = telepot.aio.DelegatorBot(bot_token, [
        include_callback_query_chat_id(
            pave_event_space())(
                per_chat_id(), create_open, Reader, timeout=50),
    ])

    loop = asyncio.get_event_loop()
    loop.create_task(MessageLoop(bot).run_forever())
    print('Listening ...')

    loop.run_forever()
