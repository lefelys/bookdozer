aiohttp==3.0.5
amqp==2.2.2
async-timeout==2.0.0
attrs==17.4.0
beautifulsoup4==4.6.0
billiard==3.5.0.3
bs4==0.0.1
celery==4.1.0
certifi==2018.1.18
chardet==3.0.4
click==6.7
Pillow==5.0.0
Django==2.0.2
gunicorn==19.7.1
idna==2.6
idna-ssl==1.0.0
kombu==4.1.0
multidict==4.1.0
pbr==3.1.1
pika==0.11.2
psycopg2==2.7.4
psycopg2-binary==2.7.4
python-dotenv==0.7.1
pytz==2018.3
requests==2.18.4
six==1.11.0
stevedore==1.28.0
telepot==12.6
urllib3==1.22
uWSGI==2.0.16
vine==1.1.4
virtualenv==15.1.0
virtualenv-clone==0.3.0
virtualenvwrapper==4.8.2
yarl==1.1.1
