# encoding=utf8
import bs4
from bs4 import BeautifulSoup, Comment, Doctype
import os
import errno
import shutil
import zipfile
import time
import datetime
import pika
import constants
import psycopg2
import json
import requests
from PIL import Image
import time

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print(method.__name__ + ':')
        print(te - ts)
        return result
    return timed

database = os.environ['DB_NAME']
user = os.environ['DB_USER']
password = os.environ['DB_PASS']
host = os.environ['DB_SERVICE']
port = os.environ['DB_PORT']
rabbit_host = os.environ['RABBIT_HOST']
bot_token = os.environ['BOT_TOKEN']
sharedFolder = os.environ['SHARED_FOLDER']

htmlToImageTagList = constants.htmlToImageTagList



def get_the_file(file_id, user_id, file_name):
    tgpath = requests.get('https://api.telegram.org/bot' + bot_token +
                          '/getFile?file_id=' + file_id).json()['result']['file_path']
    link = 'https://api.telegram.org/file/bot' + bot_token + '/' + tgpath
    response = requests.get(link, stream=True)
    directory = sharedFolder + '/inputbooks/' + str(user_id)
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    with open(directory + '/' + file_name, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response

def resize_image(filename):
    image = Image.open(filename)
    size = (700, 700)
    image.thumbnail(size, Image.ANTIALIAS)
    background = Image.new('RGB', size, (255, 255, 255, 0))
    background.paste(
    image, (int((size[0] - image.size[0]) / 2), int((size[1] - image.size[1]) / 2))
    )
    del image
    background.save(filename)
    del background

class Book:
    def __init__(self, file_name, user_id, first_name, last_name, username, time_read, chat_id):
        self.time_read = time_read
        self.file_name = file_name
        self.user_id = user_id
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.inputBooksFolder = sharedFolder + '/inputbooks/'

        try:
            os.makedirs(self.inputBooksFolder)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        fullBookPath = self.inputBooksFolder + \
            str(self.user_id) + '/' + file_name
        opfPath = self.get_opf_file_path(
            fullBookPath) # opf-file (epub-map)

        self.metadata = self.get_metadata(fullBookPath, opfPath)
        self.outputBooksDirectory = sharedFolder + '/' + str(self.user_id) + \
            '/' + \
            self.metadata.get('title', file_name).replace(
                ':', '').replace("'", '').replace(' ', '-') + '/'

        try:
            os.makedirs(self.outputBooksDirectory + 'images/')
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        self.container = self.container_builder(fullBookPath, opfPath)
        self.insert_into_db(
            self.metadata.get('title', file_name).replace(':', '').replace("'", '').replace(' ', '-'),
            self.container,
            datetime.date.today(),
            self.user_id,
            first_name,
            last_name,
            username,
            chat_id,
            time_read
            )

    # @timeit
    def container_builder(self, fullBookPath, opfPath):
        archive = zipfile.ZipFile(fullBookPath, "r")
        orderedBookFilesList, imagePaths = self.get_ordered_book_files(
            fullBookPath, opfPath)
        container = ''
        prefixAddress = self.get_prefix_address(opfPath)
        part = ''
        for file in orderedBookFilesList:
            part += archive.read(prefixAddress + file).decode()
        soup = BeautifulSoup(part, "html.parser")
        for q in soup.find_all('img'):
            oldSource = q['src']
            if ':' in oldSource:  # web-images
                continue
            imgLinkPrefix = '/media/' + str(self.user_id) + '/' + self.metadata.get(
                'title', self.file_name).replace(':', '').replace("'", '').replace(' ', '-')
            q['src'] = imgLinkPrefix + '/' + 'images/' + \
                q['src'].split('/')[-1]  # images address fixer
        for i in imagePaths:
            dataImg = archive.read(prefixAddress + i)
            path = self.outputBooksDirectory + 'images/' + i.split('/')[-1]
            with open(path, "wb") as writeFile:
                writeFile.write(dataImg)  # store images
            try:
                resize_image(path)
            except:
                pass # !!!!! to-do: delete image
        container = self.clean_code(soup.prettify())
        return container

    def get_opf_file_path(self, fullBookPath):
        archive = zipfile.ZipFile(fullBookPath, "r")
        container = archive.read('META-INF/container.xml')
        containerSoup = BeautifulSoup(container, "html.parser")
        opfPath = containerSoup.find('rootfile')['full-path']
        return opfPath

    def get_metadata(self, fullBookPath, opf_file_path):
        ''' Get metadata from book in dict '''
        archive = zipfile.ZipFile(fullBookPath, "r")
        opfData = archive.read(opf_file_path)
        soup = BeautifulSoup(opfData, "html.parser")
        meta = soup.metadata
        if not meta:
            meta = soup.find('opf:metadata')
        # print([tag.name for tag in soup.find_all()])
        metadata = {tag.name.split(':')[-1]: BeautifulSoup(str(tag), "html.parser").get_text(
        ) for tag in meta if tag != '\n' and tag.name != None and tag.name != 'meta' and ':' in tag.name}
        return metadata

    def get_prefix_address(self, opf_file_path):
        ''' Prefix folder'''
        prefixAddress = '/'.join(opf_file_path.split('/')
                                 [:(len(opf_file_path.split('/')) - 1)]) + '/'
        if prefixAddress == '/':
            prefixAddress = ''
        return prefixAddress

    def get_ordered_book_files(self, fullBookPath, opf_file_path):
        ''' Dictionary with IDs and book files paths '''
        archive = zipfile.ZipFile(fullBookPath, "r")
        filePathsWithIds = {}
        imagePaths = []
        opf = archive.read(opf_file_path)
        containerSoup = BeautifulSoup(opf, "html.parser")
        for item in containerSoup.find_all('item'):
            fileType = item['media-type']
            if fileType in ['image/png', 'image/jpeg', 'image/gif']:
                imagePaths.append(item['href'])
            filePathsWithIds[item['id']] = item['href']
        ''' Ordered book files paths '''
        orderedBookFilesList = []
        for item in containerSoup.find_all('itemref'):
            try:
                orderedBookFilesList.append(filePathsWithIds[item['idref']])
            except KeyError:
                continue
        return orderedBookFilesList, imagePaths

    # @timeit
    def clean_code(self, container):
        soup = BeautifulSoup(container, "html.parser")
        for comment in soup.findAll(text=lambda text: isinstance(text, Comment)):
            comment.extract()  # comments removing
        for e in soup:
            if isinstance(e, bs4.element.ProcessingInstruction):
                e.extract()
        for e in soup.contents:
            if isinstance(e, Doctype):
                e.extract()
        for q in soup.find_all():
            if q.name in constants.unwrapTaglist or q.name in constants.htmlToImageTagList:
            # !!!! htmlToImageTagList must be fixed !!!!
                q.unwrap()  # unwraping tags
            if q.name in constants.deleteTaglist:
                q.extract()  # removing tags with its content
            r = set()
            for z in q.attrs:
                if q.name in constants.telegraphTags:
                    if q.name == 'a':
                        if z == 'href':
                            if '#' in q['href']:
                                q.unwrap()
                    if z in constants.telegraphTags[q.name]:
                        continue
                    else:
                        r.add(z)
                else:
                    r.add(z)
            for attr in r:
                del q[attr]  # removing attributes
            if q.name in constants.replaceTagList.keys():
                q.name = constants.replaceTagList[q.name]  # replacing tags
        return soup.prettify()

    def insert_into_db(self, book_name, book_data, date, user_id, first_name, last_name, username, chat_id, time_read):
        connect = psycopg2.connect(
            database=database, user=user, host=host, password=password, port=port)
        cursor = connect.cursor()

        cursor.execute('''
            INSERT INTO bookdozes_user (id, first_name, last_name, username)
            VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING;
            ''',
                       (user_id, first_name, last_name, username))
        cursor.execute('''
            INSERT INTO bookdozes_book (book_name, book_data, date, user_id, chat_id, status, readtime)
            VALUES (%s, %s, %s, %s, %s, %s, %s);
            ''',
                       (book_name, book_data, date, user_id, chat_id, True, time_read))
        connect.commit()



if __name__ == '__main__':
    Book('lll.epub',
        user_id=19618479,
        first_name='Эмиль',
        last_name='',
        username='Lefelys',
        time_read=20,
        chat_id=19618479)

