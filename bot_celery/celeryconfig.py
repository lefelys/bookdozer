import os
from celery.schedules import crontab


rabbit_host = os.environ['RABBIT_HOST']

## Broker settings.
broker_url = 'amqp://guest:guest@' + rabbit_host

# List of modules to import when the Celery worker starts.
imports = ('tasks',)

## Using the database to store task state and results.
result_backend = 'rpc://'
result_persistent = True

task_routes = {
    'tasks.book_processor': 'book_processor_queue',
    'tasks.handle_book_result': 'handle_book_result_queue',
    'tasks.bookchimp': 'bookchimp_queue'
}


