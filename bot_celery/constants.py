unwrapTaglist = {'div', 'span', 'body', 'html', 'basefont',
                 'big', 'center', 'dd', 'dl', 'details', 'dfn',
                 'fieldset', 'form', 'font', 'header', 'hgroup', 'ins',
                 'kbd', 'main', 'mark', 'nav', 'nobr', 'plaintext', 'q',
                 's', 'samp', 'section', 'small', 'strike', 'style', 'sub',
                 'summary', 'sup', 'textarea', 'tt', 'wbr', 'xmp'}

deleteTaglist = {'applet', 'map', 'area', 'head',
                 'base', 'bgsound', 'button', 'canvas', 'menu', 'command',
                 'comment', 'dir', 'input', 'datalist', 'embed', 'frameset',
                 'frame', 'isindex', 'keygen', 'label', 'legend', 'link',
                 'map', 'marquee', 'meta', 'meter', 'noembed', 'noframes',
                 'noscript', 'object', 'select', 'optgroup', 'option',
                 'output', 'param', 'progress', 'rt', 'rp', 'ruby',
                 'script', 'time', 'title', 'var', 'del', 'svg'}

htmlToImageTagList = {'table', 'tbody', 'td',
                      'tr', 'tfoot', 'th', 'thead'}

replaceTagList = {'h1': 'h3', 'h2': 'h3'}

telegraphTags = {'article': [None], 'a': ['href'], 'aside': [None], 'b': [None],
                 'blockquote': [None], 'br': [None], 'code': [None], 'em': [None],
                 'figcaption': [None], 'figure': [None], 'h3': [None], 'h4': [None],
                 'hr': [None], 'i': [None], 'iframe': ['src'],
                 'li': [None], 'ol': [None], 'p': [None], 'pre': [None], 's': [None],
                 'strong': [None], 'u': [None], 'ul': [None], 'video': ['src'], 'audio': ['src'],
                 'source': ['src'], 'h3': [None], 'h4': [None],
                 'h5': [None], 'h6': [None], 'anchor': ['name'], 'cite': [None], 'slideshow': [None],
                 'footer': [None], 'pre': [None], 'img': ['src']}

htmlTags = {'svg', 'slideshow', 'a', 'abbr', 'acronym', 'address', 'applet', 'area',
            'article', 'aside', 'anchor', 'audio', 'b', 'base', 'basefont', 'bdi',
            'bdo', 'bgsound', 'blockquote', 'big', 'body', 'blink', 'br',
            'button', 'canvas', 'caption', 'center', 'cite', 'code', 'col',
            'colgroup', 'command', 'comment', 'datalist', 'dd', 'del',
            'details', 'dfn', 'dir', 'div', 'dl', 'dt', 'em', 'embed',
            'fieldset', 'figcaption', 'figure', 'font', 'form', 'footer',
            'frame', 'frameset', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'head',
            'header', 'hgroup', 'hr', 'html', 'i', 'iframe', 'img', 'input',
            'ins', 'isindex', 'kbd', 'keygen', 'label', 'legend', 'li', 'link',
            'main', 'map', 'marquee', 'mark', 'menu', 'meta', 'meter', 'nav',
            'nobr', 'noembed', 'noframes', 'noscript', 'object', 'ol',
            'optgroup', 'option', 'output', 'p', 'param', 'plaintext', 'pre',
            'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'script',
            'section', 'select', 'small', 'span', 'source', 'strike', 'strong',
            'style', 'sub', 'summary', 'sup', 'table', 'tbody', 'td',
            'textarea', 'tfoot', 'th', 'thead', 'time', 'title', 'tr', 'tt',
            'u', 'ul', 'var', 'video', 'wbr', 'xmp', 'image'}

