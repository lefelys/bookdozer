from celery import Celery
from celery.schedules import crontab
import time
import telepot
import psycopg2
import os
from celery.utils.log import get_task_logger
from book_parser import Book, get_the_file
from book_chimp import (get_books_on_send, update_container__add_part,
                        get_book_data__readtime__chat_id, splitter, end_book, recreate_book)
import celeryconfig


logger = get_task_logger(__name__)

app = Celery('bookdozer')
app.config_from_object(celeryconfig)


bot_token = os.environ['BOT_TOKEN']
db = os.environ['DB_NAME']
user = os.environ['DB_USER']
pswrd = os.environ['DB_PASS']
host = os.environ['DB_SERVICE']
port = os.environ['DB_PORT']


bot = telepot.Bot(bot_token)

@app.task(ignore_result=True)
def handle_book_result(chat_id, file_name, result):
    if result:
        bot.sendMessage(chat_id, f'Hey! Your book "{file_name}" is ready!')
    else:
        bot.sendMessage(chat_id, f'Whoops! Bad news. We have some problems with your book "{file_name}". But we have your book-file and will fix problem as soon as posible, so try later please')


@app.task(ignore_result=True)
def book_processor(user_id, first_name, last_name, username, file_id, file_name, time_read, chat_id, chat_type):
    try:
        get_the_file(file_id, user_id, file_name)
        book = Book(file_name, user_id, first_name,
                last_name, username, time_read, chat_id)
        app.send_task(
            'tasks.handle_book_result',
            args=[chat_id, file_name, True],
            kwargs={},
            queue='handle_book_result_queue'
            )
    except Exception as e:
        logger.exception(e)
        app.send_task(
            'tasks.handle_book_result',
            args=[chat_id, file_name, False],
            kwargs={},
            queue='handle_book_result_queue'
            )

@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        crontab(hour=15, minute=10),
        bookchimp.s(),
        name = 'daily_bookchimp',
        queue = 'bookchimp_queue',
        )

@app.task
def bookchimp(ignore_result=True):
    connect = psycopg2.connect(
        database=db, user=user, host=host, password=pswrd, port=port)
    mail_list = get_books_on_send(connect)
    connect.close()
    if mail_list:
        for i in mail_list:
            connect = psycopg2.connect(
                database=db,
                user=user,
                host=host,
                password=pswrd,
                port=port
                )
            bookID = i[0]
            book_data, readtime, chat_id = get_book_data__readtime__chat_id(
                connect, bookID)
            newContainer, part = splitter(book_data, readtime)
            if not newContainer:
                end_book(connect, bookID)
                recreate_book(connect, bookID)
                connect.close()
                continue
            part_number = update_container__add_part(
                connect, bookID, newContainer, part)  # not commited
            try:
                bot.sendMessage(
                    chat_id, f'https://t.me/iv?url=http%3A%2F%2Fbookdozer.lefelys.com%2Fbookdozes%2F{bookID}%2F{part_number}%2F&rhash=ac8f7c8eddfba0')
                time.sleep(0.03)
            except Exception as e:
                logger.exception(e)
                connect.rollback()  # rolled back update_container__add_part()
                connect.close()
            else:
                connect.commit()  # commited update_container__add_part()
                connect.close()



